# Rounded corners for xmonad

WARNING: This is not considered 'working' and I don't plan to make it work.

# Installation

```
git clone https://gitlab.com/FinnBender/xmonad-shaped  
cd xmonad-shaped  
cabal install --user
```

And then add the following code to the appropriate sections in your xmonad.hs:

```
import XMonad.Layout.ShapedWindows  

myLayout = shapedWindows (roundedRect cornerRadius) $ ...  
 where  
  cornerRadius = fromIntegral 8 :: Dimension  
```
